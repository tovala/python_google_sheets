from __future__ import division
import numpy as np
import math
import datetime

VL_INFORMATIONAL = 1
VL_DEBUG = 2
VL_WARNING = 3
VL_ERROR = 4

verbose = VL_DEBUG

# If modifying these scopes, delete the file token.pickle.
SS_SCOPES = ['https://www.googleapis.com/auth/spreadsheets']
DR_SCOPES = ['https://www.googleapis.com/auth/drive']
mealsDBFolderID = '1LdpEocbzf9B-mnJdv16o_xx1RLdsiIMi'
mealTemplateFolderID = '1A8Gap8bSvxkdQiFRHTm75ULa56EuhlM-'
partsDBFolderID = '1E9Pf6E1iH5pfjnZsfC0ryyswbJqnmHb6'
partTemplateFolderID = '1DIWmrQWBCg_81UgF9EHAa_YMnr7n0VMZ'
culinaryDrive = '0AGN3ve50-iuSUk9PVA'

COLOR_YELLOW = {"red": 255.0/256, "blue": 175.0/256, "green": 255.0/256}
COLOR_LIGHT_GRAY = {"red": 217.0/256, "blue": 217.0/256, "green": 217.0/256}
COLOR_LIGHT_BLUE = {"red": 201.0/256, "blue": 248.0/256, "green": 218.0/256}
COLOR_LIGHT_GREEN = {"red": 183.0/256, "blue": 205.0/256, "green": 225.0/256}
COLOR_LIGHT_RED = {"red": 224.0/256, "blue": 102.0/256, "green": 102.0/256}
COLOR_SALMON = {"red": 230.0/256, "blue": 183.0/256, "green": 184.0/256}
COLOR_DARK_GRAY_3 = {"red": 102.0/256, "blue": 102.0/256, "green": 102.0/256}
COLOR_DARK_GRAY_2 = {"red": 153.0/256, "blue": 153.0/256, "green": 153.0/256}
COLOR_BLACK = {"red": 1.0/256, "blue": 1.0/256, "green": 1.0/256}
COLOR_FOREST_GREEN = {"red": 217.0/256, "blue": 211.0/256, "green": 234.0/256}
COLOR_PURPLE = {"red": 153.0/256, "blue": 255.0/256, "green": 0.0/256}
COLOR_DARK_GREEN_1 = {"red": 106.0/256, "blue": 79.0/256, "green": 168.0/256}
COLOR_LIGHT_YELLOW_3 = {"red": 255.0/256, "blue": 204.0/256, "green": 242.0/256}
COLOR_LIGHT_ORANGE_1 = {"red": 246.0/256, "blue": 107.0/256, "green": 178.0/256}
COLOR_RED = {"red": 255.0/256, "blue": 0.0/256, "green": 0.0/256}
COLOR_BLUE = {"red": 0.0/256, "blue": 255.0/256, "green": 0.0/256}
COLOR_LIGHT_PURPLE_3 = {"red": 217.0/256, "blue": 233.0/256, "green": 210.0/256}

cul_tools_ranges = [
    {
        "name": "misevala_meal_id",
        "sheet": "meal_data",
        "range": "Meal Details!B2"
    },
    {
        "name": "active_version",
        "sheet": "meal_data",
        "range": "Meal Details!B3"
    },
    {
        "name": "internal_title",
        "sheet": "meal_data",
        "range": "Meal Details!B4"
    },
    {
        "name": "marketing_title",
        "sheet": "meal_data",
        "range": "Meal Details!B5"
    },
    {
        "name": "sub_title",
        "sheet": "meal_data",
        "range": "Meal Details!B6"
    },
    {
        "name": "price",
        "sheet": "meal_data",
        "range": "Meal Details!B8"
    },
    {
        "name": "terms_produced_1",
        "sheet": "meal_data",
        "range": "Meal Details!A15:E1000"
    },
    {
        "name": "terms_produced_2",
        "sheet": "meal_data",
        "range": "Meal Details!G15:V1000"
    },
    {
        "name": "valid_versions",
        "sheet": "meal_data",
        "range": "Reference!A2:B100"
    },
    {
        "name": "internal_title",
        "sheet": "meal_version",
        "range": "Culinary/Ops!B2"
    },
    {
        "name": "version_number",
        "sheet": "meal_version",
        "range": "Culinary/Ops!B3"
    },
    {
        "name": "version_date",
        "sheet": "meal_version",
        "range": "Culinary/Ops!B4"
    },
    {
        "name": "version_description",
        "sheet": "meal_version",
        "range": "Culinary/Ops!B5"
    },
    {
        "name": "misevala_version_id",
        "sheet": "meal_version",
        "range": "Culinary/Ops!B6"
    },
    {
        "name": "meal_data_spreadsheet",
        "sheet": "meal_version",
        "range": "Culinary/Ops!B8"
    },
    {
        "name": "author",
        "sheet": "meal_version",
        "range": "Culinary/Ops!B9"
    },
    {
        "name": "main_id",
        "sheet": "meal_version",
        "range": "Marketing!L2"
    },
    {
        "name": "side_id",
        "sheet": "meal_version",
        "range": "Marketing!L3"
    },
    {
        "name": "bangarang_cook_cycle_name",
        "sheet": "meal_version",
        "range": "Culinary/Ops!B11"
    },
    {
        "name": "meal_format",
        "sheet": "meal_version",
        "range": "Culinary/Ops!B12"
    },
    {
        "name": "cook_time",
        "sheet": "meal_version",
        "range": "Culinary/Ops!B13"
    },
    {
        "name": "assembly_component_1_bom",
        "sheet": "meal_version",
        "range": "Culinary/Ops!B18:B21"
    },
    {
        "name": "assembly_component_2_bom",
        "sheet": "meal_version",
        "range": "Culinary/Ops!B22:B25"
    },
    {
        "name": "assembly_component_3_bom",
        "sheet": "meal_version",
        "range": "Culinary/Ops!B26:B29"
    },
    {
        "name": "assembly_garnish_components_bom",
        "sheet": "meal_version",
        "range": "Culinary/Ops!B30:B35"
    },
    {
        "name": "assembly_component_1_weights",
        "sheet": "meal_version",
        "range": "Culinary/Ops!H18:H21"
    },
    {
        "name": "assembly_component_2_weights",
        "sheet": "meal_version",
        "range": "Culinary/Ops!H22:H25"
    },
    {
        "name": "assembly_component_3_weights",
        "sheet": "meal_version",
        "range": "Culinary/Ops!H26:H29"
    },
    {
        "name": "assembly_garnish_components_weights",
        "sheet": "meal_version",
        "range": "Culinary/Ops!H30:H35"
    },
    {
        "name": "assembly_component_1_details",
        "sheet": "meal_version",
        "range": "Culinary/Ops!I18:N18"
    },
    {
        "name": "assembly_component_2_details",
        "sheet": "meal_version",
        "range": "Culinary/Ops!I22:N22"
    },
    {
        "name": "assembly_component_3_details",
        "sheet": "meal_version",
        "range": "Culinary/Ops!I26:N26"
    },
    {
        "name": "assembly_garnish_components_details",
        "sheet": "meal_version",
        "range": "Culinary/Ops!I30:N35"
    },
    {
        "name": "assembly_procedure_block",
        "sheet": "meal_version",
        "range": "Culinary/Ops!E40:E49"
    },
    {
        "name": "ops_approval",
        "sheet": "meal_version",
        "range": "Culinary/Ops!J10"
    },
    {
        "name": "tags_protein_type",
        "sheet": "meal_version",
        "range": "Marketing!B7:E7"
    },
    {
        "name": "tags_protein_format",
        "sheet": "meal_version",
        "range": "Marketing!B8:E8"
    },
    {
        "name": "tags_side_type",
        "sheet": "meal_version",
        "range": "Marketing!B9:E9"
    },
    {
        "name": "tags_cuisine",
        "sheet": "meal_version",
        "range": "Marketing!B10:E10"
    },
    {
        "name": "tags_dish_format",
        "sheet": "meal_version",
        "range": "Marketing!B11:E11"
    },
    {
        "name": "tags_meal_type",
        "sheet": "meal_version",
        "range": "Marketing!B12:E12"
    },
    {
        "name": "tags_cooking_format",
        "sheet": "meal_version",
        "range": "Marketing!B13:E13"
    },
    {
        "name": "tags_marketing",
        "sheet": "meal_version",
        "range": "Marketing!K7:N7"
    },
    {
        "name": "tags_dietary",
        "sheet": "meal_version",
        "range": "Marketing!K8:N8"
    },
    {
        "name": "culinary_dev_status",
        "sheet": "meal_version",
        "range": "Culinary Development!G2"
    },
    {
        "name": "culinary_approval",
        "sheet": "meal_version",
        "range": "Culinary Development!G3"
    },
    {
        "name": "culinary_shelf_life",
        "sheet": "meal_version",
        "range": "Culinary Development!L3"
    },
    {
        "name": "freeze_thaw_to_failure",
        "sheet": "meal_version",
        "range": "Culinary Development!L4"
    },
    {
        "name": "development_component_1_bom",
        "sheet": "meal_version",
        "range": "Culinary Development!B10:B13"
    },
    {
        "name": "development_component_2_bom",
        "sheet": "meal_version",
        "range": "Culinary Development!B14:B17"
    },
    {
        "name": "development_component_3_bom",
        "sheet": "meal_version",
        "range": "Culinary Development!B18:B21"
    },
    {
        "name": "development_garnish_components_bom",
        "sheet": "meal_version",
        "range": "Culinary Development!B22:B27"
    },
    {
        "name": "development_component_1_weights",
        "sheet": "meal_version",
        "range": "Culinary Development!E10:E13"
    },
    {
        "name": "development_component_2_weights",
        "sheet": "meal_version",
        "range": "Culinary Development!E14:E17"
    },
    {
        "name": "development_component_3_weights",
        "sheet": "meal_version",
        "range": "Culinary Development!E18:E21"
    },
    {
        "name": "development_garnish_components_weights",
        "sheet": "meal_version",
        "range": "Culinary Development!E22:E27"
    },
    {
        "name": "calories",
        "sheet": "meal_version",
        "range": "Nutritional Info!B3"
    },
    {
        "name": "total_fat",
        "sheet": "meal_version",
        "range": "Nutritional Info!B4"
    },
    {
        "name": "sat_fat",
        "sheet": "meal_version",
        "range": "Nutritional Info!B5"
    },
    {
        "name": "cholesterol",
        "sheet": "meal_version",
        "range": "Nutritional Info!B6"
    },
    {
        "name": "sodium",
        "sheet": "meal_version",
        "range": "Nutritional Info!B7"
    },
    {
        "name": "carbs",
        "sheet": "meal_version",
        "range": "Nutritional Info!B8"
    },
    {
        "name": "sugar",
        "sheet": "meal_version",
        "range": "Nutritional Info!B9"
    },
    {
        "name": "sugar",
        "sheet": "meal_version",
        "range": "Nutritional Info!B9"
    },
    {
        "name": "protein",
        "sheet": "meal_version",
        "range": "Nutritional Info!B10"
    },
    {
        "name": "smartpoints",
        "sheet": "meal_version",
        "range": "Nutritional Info!B11"
    },
    {
        "name": "allergens",
        "sheet": "meal_version",
        "range": "Nutritional Info!F3:F8"
    },
    {
        "name": "post_cook_garnishes",
        "sheet": "meal_version",
        "range": "Nutritional Info!B16:D19"
    },
    {
        "name": "ingredient_deck",
        "sheet": "meal_version",
        "range": "Nutritional Info!B22"
    },

]

cul_tools_part_ranges = [
    {
        "name": "misevala_part_id",
        "sheet": "part_data",
        "range": "Part Details!B2"
    },
    {
        "name": "active_version",
        "sheet": "part_data",
        "range": "Part Details!B3"
    },
    {
        "name": "internal_title",
        "sheet": "part_data",
        "range": "Part Details!B5"
    },
    {
        "name": "valid_versions",
        "sheet": "part_data",
        "range": "Reference!A2:B100"
    },
    {
        "name": "internal_title",
        "sheet": "part_version",
        "range": "Culinary/Ops!B2"
    },
    {
        "name": "version_number",
        "sheet": "part_version",
        "range": "Culinary/Ops!B3"
    },
    {
        "name": "version_date",
        "sheet": "part_version",
        "range": "Culinary/Ops!B4"
    },
    {
        "name": "version_description",
        "sheet": "part_version",
        "range": "Culinary/Ops!B5"
    },
    {
        "name": "misevala_version_id",
        "sheet": "part_version",
        "range": "Culinary/Ops!B6"
    },
    {
        "name": "part_data_spreadsheet",
        "sheet": "part_version",
        "range": "Culinary/Ops!B8"
    },
    {
        "name": "author",
        "sheet": "part_version",
        "range": "Culinary/Ops!B9"
    },
    {
        "name": "part_bom",
        "sheet": "part_version",
        "range": "Culinary/Ops!A16:A35"
    },
    {
        "name": "part_weights",
        "sheet": "part_version",
        "range": "Culinary/Ops!C16:C35"
    },
    {
        "name": "yield_per_batch",
        "sheet": "part_version",
        "range": "Culinary/Ops!K16"
    },
    {
        "name": "part_procedure_block",
        "sheet": "part_version",
        "range": "Culinary/Ops!E40:E48"
    },
    {
        "name": "culinary_dev_status",
        "sheet": "part_version",
        "range": "Culinary Development!H2"
    },
    {
        "name": "culinary_approval",
        "sheet": "part_version",
        "range": "Culinary Development!H3"
    },
    {
        "name": "development_part_bom",
        "sheet": "part_version",
        "range": "Culinary Development!A10:A29"
    },
    {
        "name": "development_part_weights",
        "sheet": "part_version",
        "range": "Culinary Development!D10:D29"
    },
    {
        "name": "allergen_info",
        "sheet": "part_version",
        "range": "Nutritional Info!B3:B8"
    }
]