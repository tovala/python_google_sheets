from __future__ import division
import numpy as np
import math
import json
import datetime
import copy
from ..constants import *
#from .. import data_import as datIn

def buildMealDataSheet(meal, versionSheetIDs, verbose = VL_WARNING):

	data = []

	for line_item in meal:
		try:
			a1range = next(item['range'] for item in cul_tools_ranges if item['name'] == line_item and item['sheet'] == "meal_data")
			data.append(
				{
					"range": a1range,
					"values": [
						[meal[line_item]]
					],
					"majorDimension": "ROWS"
				}
			)
		except StopIteration:
			if(verbose <= VL_DEBUG):
				print("Did Not Find %s in Available Ranges" % line_item)

	#Add in Version Information
	vals = []
	for version in versionSheetIDs:
		vals.append([version, "https://docs.google.com/spreadsheets/d/" + versionSheetIDs[version] + "/edit?usp=drivesdk"])
	try:
		a1range = next(item['range'] for item in cul_tools_ranges if item['name'] == "valid_versions" and item['sheet'] == "meal_data")
		data.append(
			{
				"range": a1range,
				"values": vals,
				"majorDimension": "ROWS"
			}
		)
	except StopIteration:
		if(verbose <= VL_WARNING):
			print("Did Not Find Valid Versions in Available Ranges")

	#Fill in data on Terms that a Meal has been offered on
	vals1 = []
	vals2 = []
	for term in meal['terms_produced']:
		vals1.append([term['term_id'],term['version_produced'],term['glaze_meal_number'],term['production_meal_code'],term["side_swap"]["pri_sec"]])
		vals2.append([term['cogs'],term['price'],term['ratings']['thumbs_up_main'],term['ratings']['thumbs_down_main'],term['ratings']['approval_rating_main'],\
			term['ratings']['thumbs_up_side'],term['ratings']['thumbs_down_side'],term['ratings']['approval_rating_side'],term['ratings']['count_meals_selected'],\
			term['ratings']['count_meals_autoselected'],term['ratings']['customers_gte_1_meal'],term['ratings']['engagement_rate'],term['ratings']['normalized_engagement_rate'],\
			term['ratings']['order_rank'],term['ratings']['autoselect_percentage'],term['ratings']['refund_percentage']])
	try:
		a1range = next(item['range'] for item in cul_tools_ranges if item['name'] == "terms_produced_1" and item['sheet'] == "meal_data")
		data.append(
			{
				"range": a1range,
				"values": vals1,
				"majorDimension": "ROWS"
			}
		)
	except StopIteration:
		if(verbose <= VL_WARNING):
			print("Did Not Find Terms_Produced_1 in Available Ranges")
	try:
		a1range = next(item['range'] for item in cul_tools_ranges if item['name'] == "terms_produced_2" and item['sheet'] == "meal_data")
		data.append(
			{
				"range": a1range,
				"values": vals2,
				"majorDimension": "ROWS"
			}
		)
	except StopIteration:
		if(verbose <= VL_WARNING):
			print("Did Not Find Terms_Produced_2 in Available Ranges")

	return data

def buildMealVersionSheets(meal, mealDataSheetID, verbose = VL_WARNING):

	data = {}

	for version in meal['versions']:
		version_data = []

		#Add basic version info to sheet
		for line_item in version:
			try:
				a1range = next(item['range'] for item in cul_tools_ranges if item['name'] == line_item and item['sheet'] == "meal_version")
				version_data.append(
					{
						"range": a1range,
						"values": [
							[version[line_item]]
						],
						"majorDimension": "ROWS"
					}
				)
			except StopIteration:
				if(verbose <= VL_DEBUG):
					print("Did Not Find %s in Available Ranges" % line_item)

		#Next add name, meal data sheet and approval information
		try:
			a1range = next(item['range'] for item in cul_tools_ranges if item['name'] == "internal_title" and item['sheet'] == "meal_version")
			version_data.append(
				{
					"range": a1range,
					"values": [
						[meal['internal_title']]
					],
					"majorDimension": "ROWS"
				}
			)
			a1range = next(item['range'] for item in cul_tools_ranges if item['name'] == "meal_data_spreadsheet" and item['sheet'] == "meal_version")
			version_data.append(
				{
					"range": a1range,
					"values": [
						["https://docs.google.com/spreadsheets/d/" + mealDataSheetID + "/edit?usp=drivesdk"]
					],
					"majorDimension": "ROWS"
				}
			)
			a1range = next(item['range'] for item in cul_tools_ranges if item['name'] == "culinary_dev_status" and item['sheet'] == "meal_version")
			version_data.append(
				{
					"range": a1range,
					"values": [
						[version['approval']['culinary_development_status']]
					],
					"majorDimension": "ROWS"
				}
			)
			a1range = next(item['range'] for item in cul_tools_ranges if item['name'] == "culinary_approval" and item['sheet'] == "meal_version")
			version_data.append(
				{
					"range": a1range,
					"values": [
						[version['approval']['culinary_approval']]
					],
					"majorDimension": "ROWS"
				}
			)
			a1range = next(item['range'] for item in cul_tools_ranges if item['name'] == "culinary_shelf_life" and item['sheet'] == "meal_version")
			version_data.append(
				{
					"range": a1range,
					"values": [
						[next(item['approval'] for item in version['approval']['tests'] if item['name'] == "shelf_life")]
					],
					"majorDimension": "ROWS"
				}
			)
			a1range = next(item['range'] for item in cul_tools_ranges if item['name'] == "freeze_thaw_to_failure" and item['sheet'] == "meal_version")
			version_data.append(
				{
					"range": a1range,
					"values": [
						[next(item['approval'] for item in version['approval']['tests'] if item['name'] == "freeze_thaw_to_failure")]
					],
					"majorDimension": "ROWS"
				}
			)
			a1range = next(item['range'] for item in cul_tools_ranges if item['name'] == "ops_approval" and item['sheet'] == "meal_version")
			version_data.append(
				{
					"range": a1range,
					"values": [
						[version['approval']['ops_approval']['approval']]
					],
					"majorDimension": "ROWS"
				}
			)
		except StopIteration:
			if(verbose <= VL_DEBUG):
				print("Could Not Complete Approval Section for Version %0d" % version['version_number'])

		#Add Tags to Meal
		for tag_group in [item for item in cul_tools_ranges if "tags" in item['name'] and item['sheet'] == "meal_version"]:
			vals = []
			for tag in version['tags']:
				if tag['type'] in tag_group['name']:
					vals.append(tag['value'])
			version_data.append(
				{
					"range": tag_group['range'],
					"values": [
						vals
					],
					"majorDimension": "ROWS"
				}
			)

		#Add Main/Side Component Info
		for i in range(1,4):
			component_bom = []
			component_weights = []

			try:
				component = next(component for component in version['bill_of_materials']['components'] if component['index'] == i)
			except StopIteration:
				continue
			component_details = [component['target_container'], component['alternate_container'], component['perf'], component['map'],"",component['main_side']]
			for item in component['bill_of_materials']:
				component_bom.append(item['name'])
				component_weights.append(item['weight'])

			for item in [item for item in cul_tools_ranges if "component_"+str(i)+"_bom" in item['name'] and item['sheet'] == "meal_version"]:
				version_data.append(
					{
						"range": item['range'],
						"values": [
							component_bom
						],
						"majorDimension": "COLUMNS"
					}
				)

			for item in [item for item in cul_tools_ranges if "component_"+str(i)+"_weights" in item['name'] and item['sheet'] == "meal_version"]:
				version_data.append(
					{
						"range": item['range'],
						"values": [
							component_weights
						],
						"majorDimension": "COLUMNS"
					}
				)
			try:
				a1range = next(item['range'] for item in cul_tools_ranges if item['name'] == "assembly_component_"+str(i)+"_details" and item['sheet'] == "meal_version")
				version_data.append(
					{
						"range": a1range,
						"values": [
							component_details
						],
						"majorDimension": "ROWS"
					}
				)
			except StopIteration:
				if(verbose <= VL_DEBUG):
					print("Did Not Find Component %0d Details in Available Ranges" % i)

		#Add Garnish Info
		garnish_bom = []
		garnish_weights = []
		garnish_details = []
		for garnish in version['bill_of_materials']['garnish_components']:
			garnish_bom.append(garnish['name'])
			garnish_weights.append(garnish['weight'])
			garnish_details.append([garnish['target_container'],garnish['alternate_container'],"","",garnish['before_after'],garnish['main_side']])
		for item in [item for item in cul_tools_ranges if "garnish_components_bom" in item['name'] and item['sheet'] == "meal_version"]:
			version_data.append(
				{
					"range": item['range'],
					"values": [
						garnish_bom
					],
					"majorDimension": "COLUMNS"
				}
			)
		for item in [item for item in cul_tools_ranges if "garnish_components_weights" in item['name'] and item['sheet'] == "meal_version"]:
			version_data.append(
				{
					"range": item['range'],
					"values": [
						garnish_weights
					],
					"majorDimension": "COLUMNS"
				}
			)
		try:
			a1range = next(item['range'] for item in cul_tools_ranges if item['name'] == "assembly_garnish_components_details" and item['sheet'] == "meal_version")
			version_data.append(
				{
					"range": a1range,
					"values": garnish_details,
					"majorDimension": "ROWS"
				}
			)
		except StopIteration:
			if(verbose <= VL_DEBUG):
				print("Did Not Find Garnish Component Details in Available Ranges")

		procedure = sorted(version['procedure'], key=lambda x: x['stepNumber'])
		procedure_vals = []
		for step in procedure:
			procedure_vals.append(step['instructions'])
		try:
			a1range = next(item['range'] for item in cul_tools_ranges if item['name'] == "assembly_procedure_block" and item['sheet'] == "meal_version")
			version_data.append(
				{
					"range": a1range,
					"values": [procedure_vals],
					"majorDimension": "COLUMNS"
				}
			)
		except StopIteration:
			if(verbose <= VL_DEBUG):
				print("Did Not Find Assembly Procedure Block in Available Ranges")

		pcgs = []
		for line_item in version['nutritional_info']:
			if(line_item == "post_cook_garnishes"):
				for sub_line in version['nutritional_info']['post_cook_garnishes']:
					pcgs.append([sub_line['name'],sub_line['calories'],''])
				try:
					a1range = next(item['range'] for item in cul_tools_ranges if item['name'] == line_item and item['sheet'] == "meal_version")
					version_data.append(
						{
							"range": a1range,
							"values": pcgs,
							"majorDimension": "ROWS"
						}
					)
				except StopIteration:
					if(verbose <= VL_DEBUG):
						print("Did Not Find %s in Available Ranges" % line_item)
				continue
			if(line_item == "allergens"):
				try:
					a1range = next(item['range'] for item in cul_tools_ranges if item['name'] == line_item and item['sheet'] == "meal_version")
					version_data.append(
						{
							"range": a1range,
							"values": [version['nutritional_info'][line_item]],
							"majorDimension": "COLUMNS"
						}
					)
				except StopIteration:
					if(verbose <= VL_DEBUG):
						print("Did Not Find %s in Available Ranges" % line_item)
				continue
			try:
				a1range = next(item['range'] for item in cul_tools_ranges if item['name'] == line_item and item['sheet'] == "meal_version")
				version_data.append(
					{
						"range": a1range,
						"values": [
							[version['nutritional_info'][line_item]]
						],
						"majorDimension": "ROWS"
					}
				)
			except StopIteration:
				if(verbose <= VL_DEBUG):
					print("Did Not Find %s in Available Ranges" % line_item)

		data[str(version['version_number'])] = version_data
	return data


def buildPartDataSheet(part, versionSheetIDs, verbose = VL_WARNING):

	data = []

	for line_item in part:
		try:
			a1range = next(item['range'] for item in cul_tools_part_ranges if item['name'] == line_item and item['sheet'] == "part_data")
			data.append(
				{
					"range": a1range,
					"values": [
						[part[line_item]]
					],
					"majorDimension": "ROWS"
				}
			)
		except StopIteration:
			if(verbose <= VL_DEBUG):
				print("Did Not Find %s in Available Ranges" % line_item)

	#Add in Version Information
	vals = []
	for version in versionSheetIDs:
		vals.append([version, "https://docs.google.com/spreadsheets/d/" + versionSheetIDs[version] + "/edit?usp=drivesdk"])
	try:
		a1range = next(item['range'] for item in cul_tools_part_ranges if item['name'] == "valid_versions" and item['sheet'] == "part_data")
		data.append(
			{
				"range": a1range,
				"values": vals,
				"majorDimension": "ROWS"
			}
		)
	except StopIteration:
		if(verbose <= VL_WARNING):
			print("Did Not Find Valid Versions in Available Ranges")

	return data

def buildPartVersionSheets(part, partDataSheetID, verbose = VL_WARNING):

	data = {}

	for version in part['versions']:
		version_data = []

		#Add basic version info to sheet
		for line_item in version:
			try:
				a1range = next(item['range'] for item in cul_tools_part_ranges if item['name'] == line_item and item['sheet'] == "part_version")
				version_data.append(
					{
						"range": a1range,
						"values": [
							[version[line_item]]
						],
						"majorDimension": "ROWS"
					}
				)
			except StopIteration:
				if(verbose <= VL_DEBUG):
					print("Did Not Find %s in Available Ranges" % line_item)

		#Next add name, part data sheet and approval information
		try:
			a1range = next(item['range'] for item in cul_tools_part_ranges if item['name'] == "internal_title" and item['sheet'] == "part_version")
			version_data.append(
				{
					"range": a1range,
					"values": [
						[part['internal_title']]
					],
					"majorDimension": "ROWS"
				}
			)
			a1range = next(item['range'] for item in cul_tools_part_ranges if item['name'] == "part_data_spreadsheet" and item['sheet'] == "part_version")
			version_data.append(
				{
					"range": a1range,
					"values": [
						["https://docs.google.com/spreadsheets/d/" + partDataSheetID + "/edit?usp=drivesdk"]
					],
					"majorDimension": "ROWS"
				}
			)
			a1range = next(item['range'] for item in cul_tools_part_ranges if item['name'] == "culinary_dev_status" and item['sheet'] == "part_version")
			version_data.append(
				{
					"range": a1range,
					"values": [
						[version['approval']['culinary_development_status']]
					],
					"majorDimension": "ROWS"
				}
			)
			a1range = next(item['range'] for item in cul_tools_part_ranges if item['name'] == "culinary_approval" and item['sheet'] == "part_version")
			version_data.append(
				{
					"range": a1range,
					"values": [
						[version['approval']['culinary_approval']]
					],
					"majorDimension": "ROWS"
				}
			)
		except StopIteration:
			if(verbose <= VL_DEBUG):
				print("Could Not Complete Approval Section for Version %0d" % version['version_number'])

		#Add BOM Info
		part_bom = []
		part_weights = []
		for line_item in version['bill_of_materials']:
			part_bom.append(line_item['name'])
			part_weights.append(line_item['weight'])
		for item in [item for item in cul_tools_part_ranges if "part_bom" in item['name'] and item['sheet'] == "part_version"]:
			version_data.append(
				{
					"range": item['range'],
					"values": [
						part_bom
					],
					"majorDimension": "COLUMNS"
				}
			)
		for item in [item for item in cul_tools_part_ranges if "part_weights" in item['name'] and item['sheet'] == "part_version"]:
			version_data.append(
				{
					"range": item['range'],
					"values": [
						part_weights
					],
					"majorDimension": "COLUMNS"
				}
			)

		procedure = sorted(version['procedure'], key=lambda x: x['stepNumber'])
		procedure_vals = []
		for step in procedure:
			procedure_vals.append(step['instructions'])
		try:
			a1range = next(item['range'] for item in cul_tools_part_ranges if item['name'] == "part_procedure_block" and item['sheet'] == "part_version")
			version_data.append(
				{
					"range": a1range,
					"values": [procedure_vals],
					"majorDimension": "COLUMNS"
				}
			)
		except StopIteration:
			if(verbose <= VL_DEBUG):
				print("Did Not Find Assembly Procedure Block in Available Ranges")

	    #TODO: Add nutritional info
		data[str(version['version_number'])] = version_data
	return data