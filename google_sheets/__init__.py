from __future__ import print_function
import pickle
import math
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from .constants import *


# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

def googleAuthV1(credentials_file,scope,token_json_file = 'token.json', serviceRequested = ['sheets','v4']):
    """
    This is the main function. Lines 321 through 344 handle authentication and which version of the sheets API to set up.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists(token_json_file):
        creds = Credentials.from_authorized_user_file(token_json_file, SCOPES)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                credentials_file, scope)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open(token_json_file, 'w') as token:
            token.write(creds.to_json())

    service = build(serviceRequested[0], serviceRequested[1], credentials=creds)

    return service

def createNewGDriveFolder(folderName, folderParent, driveApp, verbose = VL_WARNING):
    file_metadata = {
        'name': folderName,
        'mimeType': 'application/vnd.google-apps.folder',
        'parents': [folderParent]
    }

    newFolder = driveApp.files().create(body=file_metadata,fields='id',supportsAllDrives = True).execute()

    if(verbose <= VL_DEBUG):
        print("Folder ID: %s" % newFolder.get('id'))

    return newFolder['id']

def getFilesInFolder(folderID, sharedDriveToSearch, driveApp, verbose = VL_WARNING):
    page_token = None
    fileIDs = []
    while True:
        listQuery = driveApp.files().list(q = "'" + folderID + "' in parents and trashed = false", fields = "nextPageToken, files(id,name)",pageToken = page_token, corpora = 'drive', driveId = sharedDriveToSearch, includeItemsFromAllDrives = True, supportsAllDrives = True).execute()
        fileList = listQuery.get('files',[])
        if(verbose <= VL_DEBUG):
            print(len(fileList))
        
        for file in fileList:
            fileIDs.append({'id': file.get('id'), 'name': file.get('name')})

        page_token = listQuery.get('nextPageToken', None)
        if(page_token is None):
            break
    return fileIDs

def batchUpdateCellWrites(data, spreadsheet_id,service):
    body = {
        'valueInputOption': "USER_ENTERED",
        'data': data
    }

    response = service.spreadsheets().values().batchUpdate(spreadsheetId = spreadsheet_id, body = body).execute()

    return response


def googleAuth():
    """
    This is the main function. Lines 321 through 344 handle authentication and which version of the sheets API to set up.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('sheets', 'v4', credentials=creds)

    return service



def writeValues(cellRange, values, service, spreadsheet_id):
    #Write values to a spreadsheet
    #Inputs are the cell range to write/write over
    #Values are the values to be written. To skip writing a value, write "None"

    body = {
        'values': values
    }
    
    result = service.spreadsheets().values().update(spreadsheetId = spreadsheet_id, range = cellRange, valueInputOption = "USER_ENTERED", body = body).execute()
    return result

def readValues(cellRange, service, spreadsheet_id):
    result = service.spreadsheets().values().get(spreadsheetId = spreadsheet_id, range = cellRange).execute()
    values = result.get('values', [])

    return values

def changeSpreadsheetName(name, service, spreadsheet_id):

    requests = []
    requests.append({
        'updateSpreadsheetProperties': {
            'properties': {
                'title': name
            },
            'fields': 'title'
        }
    })

    body = {
        'requests': requests
    }
    
    response = service.spreadsheets().batchUpdate(spreadsheetId = spreadsheet_id, body = body).execute()

    return response

def addSheet(sheet_name, service, spreadsheet_id):

    requests = []
    requests.append({
        'addSheet': {
            'properties': {
                'title': sheet_name
            }
        }
    })
    
    body = {
        'requests': requests
    }
    
    response = service.spreadsheets().batchUpdate(spreadsheetId = spreadsheet_id, body = body).execute()

    return response

def getSheetIDFromName(sheetName,service,spreadsheet_id):
    spreadsheetInfo = service.spreadsheets().get(spreadsheetId = spreadsheet_id).execute()
    for item in spreadsheetInfo['sheets']:
        if item['properties']['title'] == sheetName:
            sheetId = item['properties']['sheetId']
    return sheetId

def duplicateSheet(sheetToDuplicate, duplicatedSheetName, service, spreadsheet_id):

    sheetId = getSheetIDFromName(sheetToDuplicate,service,spreadsheet_id)    

    requests = []
    requests.append({
        'duplicateSheet': {
            'sourceSheetId': sheetId,
            'insertSheetIndex': 0,
            'newSheetName': duplicatedSheetName
        }
    })
    
    body = {
        'requests': requests
    }
    
    response = service.spreadsheets().batchUpdate(spreadsheetId = spreadsheet_id, body = body).execute()

    return response

def findReplace(sheetToScan, stringToFind, replacementString, formula, service, spreadsheet_id):
    sheetId = getSheetIDFromName(sheetToScan,service,spreadsheet_id)    

    requests = []
    requests.append({
        'findReplace': {
            'find': stringToFind,
            'replacement': replacementString,
            'includeFormulas': formula,
            'sheetId': sheetId
        }
    })
    
    body = {
        'requests': requests
    }
    
    response = service.spreadsheets().batchUpdate(spreadsheetId = spreadsheet_id, body = body).execute()

    return response

def repeatCell(sheet, startRowIndex, endRowIndex, startColumnIndex, endColumnIndex, value, service, spreadsheet_id):
    sheetId = getSheetIDFromName(sheet,service,spreadsheet_id)    

    requests = []
    requests.append({
        'repeatCell': {
            'range': {
                'sheetId': sheetId,
                'startRowIndex': startRowIndex,
                'endRowIndex': endRowIndex,
                'startColumnIndex': startColumnIndex,
                'endColumnIndex': endColumnIndex
            },
            'cell': {
                'userEnteredValue': {
                    'formulaValue': value
                }
            },
            'fields': 'userEnteredValue'
        }
    })
    
    body = {
        'requests': requests
    }
    
    response = service.spreadsheets().batchUpdate(spreadsheetId = spreadsheet_id, body = body).execute()

    return response



def changeSheetName(sheetToChange, newSheetName, service, spreadsheet_id):

    sheetId = getSheetIDFromName(sheetToChange,service,spreadsheet_id)    

    requests = []
    requests.append({
        'updateSheetProperties': {
            'properties': {
                'sheetId': sheetId,
                'title': newSheetName
            },
            'fields': 'title'
        }
    })
    
    body = {
        'requests': requests
    }
    
    response = service.spreadsheets().batchUpdate(spreadsheetId = spreadsheet_id, body = body).execute()

    return response

def hideSheet(sheetToHide, service, spreadsheet_id):

    sheetId = getSheetIDFromName(sheetToHide,service,spreadsheet_id)    

    requests = []
    requests.append({
        'updateSheetProperties': {
            'properties': {
                'sheetId': sheetId,
                'hidden': True
            },
            'fields': 'hidden'
        }
    })
    
    body = {
        'requests': requests
    }
    
    response = service.spreadsheets().batchUpdate(spreadsheetId = spreadsheet_id, body = body).execute()

    return response

def colorCellRangeRed(sheet, startRowIndex, endRowIndex, startColumnIndex, endColumnIndex, service, spreadsheet_id):
    sheetId = getSheetIDFromName(sheet,service,spreadsheet_id)    

    requests = []
    requests.append({
        'repeatCell': {
            'range': {
                'sheetId': sheetId,
                'startRowIndex': startRowIndex,
                'endRowIndex': endRowIndex,
                'startColumnIndex': startColumnIndex,
                'endColumnIndex': endColumnIndex
            },
            'cell': {
                'userEnteredFormat': {
                    'backgroundColor': {
                        "red": 1.0,
                        "green": 0.0,
                        "blue": 0.0
                    }
                }
            },
            "fields": "userEnteredFormat(backgroundColor)"
        }
    })
    
    body = {
        'requests': requests
    }
    
    response = service.spreadsheets().batchUpdate(spreadsheetId = spreadsheet_id, body = body).execute()

    return response

def colorCellRange(sheet, color, startRowIndex, endRowIndex, startColumnIndex, endColumnIndex, service, spreadsheet_id):
    sheetId = getSheetIDFromName(sheet,service,spreadsheet_id)    

    if(color == 'Dark Green'):
        bgColor = {"red": 0.0, "green": 100.0/256, "blue": 0.0}
    elif(color == 'Light Green'):
        bgColor = {"red": 0.0, "green": 255.0/256, "blue": 0.0}
    elif(color == 'Blue'):
        bgColor = {"red": 0.0, "green": 0.0, "blue": 255.0/256}
    elif(color == 'Yellow'):
        bgColor = {"red": 255.0/256, "green": 255.0/256, "blue": 0.0}
    elif(color == 'Orange'):
        bgColor = {"red": 255.0/256, "green": 165.0/256, "blue": 0.0}
    elif(color == 'Light Red'):
        bgColor = {"red": 255.0/256, "green": 0.0, "blue": 0.0}
    elif(color == 'Dark Red'):
        bgColor = {"red": 139.0/256, "green": 0.0, "blue": 0.0}
    else:
        print('Color Not Recognized, Ignoring Request')
        return []

    requests = []
    requests.append({
        'repeatCell': {
            'range': {
                'sheetId': sheetId,
                'startRowIndex': startRowIndex,
                'endRowIndex': endRowIndex,
                'startColumnIndex': startColumnIndex,
                'endColumnIndex': endColumnIndex
            },
            'cell': {
                'userEnteredFormat': {
                    'backgroundColor': bgColor
                }
            },
            "fields": "userEnteredFormat(backgroundColor)"
        }
    })
    
    body = {
        'requests': requests
    }
    
    response = service.spreadsheets().batchUpdate(spreadsheetId = spreadsheet_id, body = body).execute()

    return response

def addBorder(sheetToEdit, rangeRowStart, rangeRowEnd, rangeColumnStart, rangeColumnEnd, service, spreadsheet_id, side = 'bottom', style = 'SOLID'):
    #Side can be 'top', 'bottom', 'left' or 'right'

    sheetId = getSheetIDFromName(sheetToEdit,service,spreadsheet_id) 

    requests = []
    requests.append({
        'updateBorders': {
            'range': {
                'sheetId': sheetId,
                'startRowIndex': rangeRowStart,
                'endRowIndex': rangeRowEnd,
                'startColumnIndex': rangeColumnStart,
                'endColumnIndex': rangeColumnEnd
            },
            side: {
                'color': {'red':0,'green':0,'blue':0},
                'style': style
            }
        }
    })
    
    body = {
        'requests': requests
    }
    
    response = service.spreadsheets().batchUpdate(spreadsheetId = spreadsheet_id, body = body).execute()

    return response

def columnIndexToLetter(col_index):
    if(col_index<=26):
        return chr(col_index+96).capitalize()
    elif(col_index<=(676+26)):
        if(col_index%26 == 0):
            return (chr(math.floor(col_index/26)-1+96).capitalize() + "Z")
        column = chr(math.floor(col_index/26)+96).capitalize() + chr(col_index%26+96).capitalize()
        return column
    else:
        print("Column Greater than ZZ, Make a Smaller Spreadsheet")
        return "A"

def A1NotationToRC(cell_range):
    cells = cell_range.split(":")
    startRow = 0
    endRow = 0
    startColumn = 0
    endColumn = 0

    try: #Determine the index of the character that begins the string for the row (index after the string that determines the column)
        index = next(i for i in range(len(cells[0])) if cells[0][i].isdigit())
        #print(index)
    except StopIteration:
        raise ValueError("Cell Range Invalid")
    if(index == 0):
        raise ValueError("Cell Range Invalid")

    try: #Convert the string for the column from alphabet to integer
        for i in range(index):
            #print(ord(cells[0][i].lower())-96)
            startColumn += (ord(cells[0][i].lower())-96)*math.pow(26,index-i-1)
            #print(startColumn)
    except:
        raise ValueError("Cell Range Invalid")
    startColumn -= 1

    try: #Convert the string for the row to an integer
        startRow = int(cells[0][index:])-1
    except ValueError:
        raise ValueError("Cell Range Invalid")
    #print(startRow)

    if(len(cells)==1):
        endRow = startRow + 1
        endColumn = startColumn + 1

    if(len(cells)==2):
        try: #Determine the index of the character that begins the string for the row (index after the string that determines the column)
            index = next(i for i in range(len(cells[1])) if cells[1][i].isdigit())
            #print(index)
        except StopIteration:
            raise ValueError("Cell Range Invalid")
        if(index == 0):
            raise ValueError("Cell Range Invalid")
    
        try: #Convert the string for the column from alphabet to integer
            for i in range(index):
                #print(ord(cells[1][i].lower())-96)
                endColumn += (ord(cells[1][i].lower())-96)*math.pow(26,index-i-1)
                #print(endColumn)
        except:
            raise ValueError("Cell Range Invalid")
    
        try: #Convert the string for the row to an integer
            endRow = int(cells[1][index:])
        except ValueError:
            raise ValueError("Cell Range Invalid")

    return [startRow, endRow, startColumn, endColumn]

def setDataValidation(sheetToEdit, rangeRowStart, rangeRowEnd, rangeColumnStart, rangeColumnEnd, service, spreadsheet_id, refRange):

    sheetId = getSheetIDFromName(sheetToEdit,service,spreadsheet_id) 

    requests = []
    requests.append({
        'setDataValidation': {
            'range': {
                'sheetId': sheetId,
                'startRowIndex': rangeRowStart,
                'endRowIndex': rangeRowEnd,
                'startColumnIndex': rangeColumnStart,
                'endColumnIndex': rangeColumnEnd
            },
            'rule': {
                "condition": {
                    'type': 'ONE_OF_RANGE',
                    'values': [
                        {'userEnteredValue':'='+refRange}
                    ]
                },
                "strict": True,
                "showCustomUi": True
            }
        }
    })
    
    body = {
        'requests': requests
    }
    
    response = service.spreadsheets().batchUpdate(spreadsheetId = spreadsheet_id, body = body).execute()

    return response

def compileDataValidationCells(sheetId, requests, row, columnStart, columnEnd, service, spreadsheet_id, vals): 

    uevs = []
    for val in vals:
        uevs.append({"userEnteredValue": val})

    requests.append({
        'setDataValidation': {
            'range': {
                'sheetId': sheetId,
                'startRowIndex': row,
                'endRowIndex': row+1,
                'startColumnIndex': columnStart,
                'endColumnIndex': columnEnd
            },
            'rule': {
                "condition": {
                    'type': 'ONE_OF_LIST',
                    'values': [
                        uevs
                    ]
                },
                "strict": True,
                "showCustomUi": True
            }
        }
    })

    return requests

def compileBordersAll(sheetId, requests, startEndRowColumn, service, spreadsheet_id): 

    requests.append({
        'updateBorders': {
            'range': {
                'sheetId': sheetId,
                'startRowIndex': startEndRowColumn[0],
                'endRowIndex': startEndRowColumn[1],
                'startColumnIndex': startEndRowColumn[2],
                'endColumnIndex': startEndRowColumn[3]
            },
            "top": {
                "style": "SOLID",
                "width": 1,
                "color": {
                    "red": 0.0,
                    "blue": 0.0,
                    "green": 0.0
                }
            },
            "bottom": {
                "style": "SOLID",
                "width": 1,
                "color": {
                    "red": 0.0,
                    "blue": 0.0,
                    "green": 0.0
                }
            },
            "left": {
                "style": "SOLID",
                "width": 1,
                "color": {
                    "red": 0.0,
                    "blue": 0.0,
                    "green": 0.0
                }
            },
            "right": {
                "style": "SOLID",
                "width": 1,
                "color": {
                    "red": 0.0,
                    "blue": 0.0,
                    "green": 0.0
                }
            },
            "innerHorizontal": {
                "style": "SOLID",
                "width": 1,
                "color": {
                    "red": 0.0,
                    "blue": 0.0,
                    "green": 0.0
                }
            },
            "innerVertical": {
                "style": "SOLID",
                "width": 1,
                "color": {
                    "red": 0.0,
                    "blue": 0.0,
                    "green": 0.0
                }
            }
        }
    })

    return requests

def compileDataValidationV1(sheetId, requests, startEndRowColumn, service, spreadsheet_id, vals, dv_type = "ONE_OF_LIST", strict = True): 

    uevs = []
    for val in vals:
        uevs.append({"userEnteredValue": val})

    requests.append({
        'setDataValidation': {
            'range': {
                'sheetId': sheetId,
                'startRowIndex': startEndRowColumn[0],
                'endRowIndex': startEndRowColumn[1],
                'startColumnIndex': startEndRowColumn[2],
                'endColumnIndex': startEndRowColumn[3]
            },
            'rule': {
                "condition": {
                    'type': dv_type,
                    'values': [
                        uevs
                    ]
                },
                "strict": strict,
                "showCustomUi": True
            }
        }
    })

    return requests

def compileBatchColorCellRangeColor(sheetId, requests, startRowIndex, endRowIndex, startColumnIndex, endColumnIndex, service, spreadsheet_id, bgColor = {"red": 175.0/256, "green": 212.0/256, "blue": 225.0/256}):    

    requests.append({
        'repeatCell': {
            'range': {
                'sheetId': sheetId,
                'startRowIndex': startRowIndex,
                'endRowIndex': endRowIndex,
                'startColumnIndex': startColumnIndex,
                'endColumnIndex': endColumnIndex
            },
            'cell': {
                'userEnteredFormat': {
                    'backgroundColor': bgColor
                }
            },
            "fields": "userEnteredFormat(backgroundColor)"
        }
    })

    return requests

def batchUpdate(requests, service, spreadsheet_id):

    body = {
        'requests': requests
    }
    
    response = service.spreadsheets().batchUpdate(spreadsheetId = spreadsheet_id, body = body).execute()

    return response

def cloneSheet(fileID, fileTitle, destFolderID, verbose = VL_WARNING):
    urlReq = 'https://misevala-api.dev.tvla.co/v0/culinary/copySheet'

    query = json.dumps( 
        
        {
            "spreadsheetID": fileID,
            "newTitle": fileTitle,
            'destFolderID': destFolderID
        }
        
    )
    
    http = url.PoolManager()
    r = http.request('POST', urlReq, body = query, headers={'Content-Type': 'application/json'})
    if(verbose <= VL_DEBUG):
        print(r.data)

    return json.loads(r.data)['newFileID']


def getFoldersInFolder(folderID, sharedDriveToSearch, driveApp, verbose = VL_WARNING):
    page_token = None
    folderIDs = []
    while True:
        listQuery = driveApp.files().list(q = "'" + folderID + "' in parents and trashed = false and mimeType='application/vnd.google-apps.folder'", fields = "nextPageToken, files(id,name)",pageToken = page_token, corpora = 'drive', driveId = sharedDriveToSearch, includeItemsFromAllDrives = True, supportsAllDrives = True).execute()
        folderList = listQuery.get('files',[])
        if(verbose <= VL_DEBUG):
            print(len(folderList))
        
        for folder in folderList:
            folderIDs.append({'id': folder.get('id'), 'name': folder.get('name')})

        page_token = listQuery.get('nextPageToken', None)
        if(page_token is None):
            break
    return folderIDs

def compileConditionalFormattingBgColor(sheetId, requests, startEndRowColumn, service, spreadsheet_id, cf_rule, color): 

    requests.append({
        'addConditionalFormatRule': {
            'rule': {
                'ranges': [{
                    'sheetId': sheetId,
                    'startRowIndex': startEndRowColumn[0],
                    'endRowIndex': startEndRowColumn[1],
                    'startColumnIndex': startEndRowColumn[2],
                    'endColumnIndex': startEndRowColumn[3]
                }],
                'booleanRule': {
                    'condition': {
                        'type': "CUSTOM_FORMULA",
                        'values': [{
                            'userEnteredValue': cf_rule
                        }]
                    },
                    "format": {
                        "backgroundColor": color
                    }
                }
            },
            "index": 0
        }
    })

    return requests

def compileBatchColorCellRangeColorV1(sheetId, requests, startEndRowColumn, service, spreadsheet_id, bgColor = {"red": 175.0/256, "green": 212.0/256, "blue": 225.0/256}):    

    requests.append({
        'repeatCell': {
            'range': {
                'sheetId': sheetId,
                    'startRowIndex': startEndRowColumn[0],
                    'endRowIndex': startEndRowColumn[1],
                    'startColumnIndex': startEndRowColumn[2],
                    'endColumnIndex': startEndRowColumn[3]
            },
            'cell': {
                'userEnteredFormat': {
                    'backgroundColor': bgColor
                }
            },
            "fields": "userEnteredFormat(backgroundColor)"
        }
    })

    return requests

def compileHideColumn(sheetId, requests, startEndRowColumn, service, spreadsheet_id, hide = True): 

    requests.append({
        'updateDimensionProperties': {
            'range': {
                'sheetId': sheetId,
                'dimension': "COLUMNS",
                'startIndex': startEndRowColumn[2],
                'endIndex': startEndRowColumn[3]
            },
            'properties': {
                'hiddenByUser': hide
            },
            'fields': 'hiddenByUser'
        }
    })

    return requests

def compileUpdateProtectedRangeAddProtectedRangeToSheet(sheetObjectToUpdate, requests, service, spreadsheet_id, rangeToAdd, users, newSheet = {"newSheet": False, "id": ""}, verbosity = VL_WARNING):

    if(newSheet['newSheet']):
        sheetId = newSheet['id']
    else:

        try:
            sheetPRs = sheetObjectToUpdate['protectedRanges']
            for PR in sheetPRs:
                if('endColumnIndex' in PR['range']):
                    if((PR['range']['sheetId'] == sheetObjectToUpdate['properties']['sheetId']) and (PR['range']['startRowIndex'] == rangeToAdd[0])\
                     and (PR['range']['endRowIndex'] == rangeToAdd[1]) and (PR['range']['startColumnIndex'] == rangeToAdd[2]) and (PR['range']['endColumnIndex'] == rangeToAdd[3])):
                        if(verbosity >= VL_WARNING):
                            print("WARNING: Protected Range Already Exists for Sheet %s" % sheetObjectToUpdate['properties']['title'])
                        return requests
        except KeyError:
            if(verbosity >= VL_INFORMATIONAL):
                print("Sheet %s Has No Protected Ranges Existing" % sheetObjectToUpdate['properties']['title'])

        sheetId = sheetObjectToUpdate['properties']['sheetId']
    

    requests.append({
        'addProtectedRange': {
            'protectedRange': {
                'range': {
                    'sheetId': sheetId,
                    'startRowIndex': rangeToAdd[0],
                    'endRowIndex': rangeToAdd[1],
                    'startColumnIndex': rangeToAdd[2],
                    'endColumnIndex': rangeToAdd[3]
                },
                'editors': {
                    'users': users
                }
            }
        }
    })

    return requests

def compileProtectSheet(sheetObjectToUpdate, requests, service, spreadsheet_id, unprotected_range, users, newSheet = {"newSheet": False, "id": ""}, verbosity = VL_WARNING): 

    if(newSheet['newSheet']):
        sheetId = newSheet['id']
    else:

        sheetProtected = False
    
        try:
            sheetPRs = sheetObjectToUpdate['protectedRanges']
            for PR in sheetPRs:
                if('endColumnIndex' not in PR['range']):
                    sheetProtected = True
        except KeyError:
            sheetProtected = False
    
        if(sheetProtected):
            print("WARNING: %s Sheet Already Protected" % sheetObjectToUpdate['properties']['title'])
            return requests

        sheetId = sheetObjectToUpdate['properties']['sheetId']

    requests.append({
        'addProtectedRange': {
            'protectedRange': {
                'range': {
                    'sheetId': sheetId
                },
                'unprotectedRanges': [{
                    'sheetId': sheetId,
                    'startRowIndex': unprotected_range[0],
                    'endRowIndex': unprotected_range[1],
                    'startColumnIndex': unprotected_range[2],
                    'endColumnIndex': unprotected_range[3]
                }],
                'editors': {
                    'users': users
                }
            }
        }
    })

    return requests

def compileHideSheet(sheetId, requests):

    requests.append({
        'updateSheetProperties': {
            'properties': {
                'sheetId': sheetId,
                'hidden': True
            },
            'fields': 'hidden'
        }
    })

    return requests

def compileUpdateSheetProtectedRangesAddUser(sheetObjectToUpdate, requests, service, spreadsheet_id, user_to_add):

    try:
        sheetPRs = sheetObjectToUpdate['protectedRanges']
    except KeyError:
        print("WARNING: Sheet %s Has No Protected Ranges" % sheetObjectToUpdate['properties']['title'])
        return requests

    for PR in sheetPRs:
        PR['editors']['users'].append(user_to_add)
        users = PR['editors']['users']
        requests.append({
            'updateProtectedRange': {
                'protectedRange': {
                    'protectedRangeId': PR['protectedRangeId'],
                    'editors': {
                        'users': users
                    }
                },
                "fields": "editors"
            }
        })

    return requests

def compileUpdateSheetProtectedRangesClonePermissions(sheetObjectToUpdate, requests, service, spreadsheet_id, user_to_clone, user_to_add):

    try:
        sheetPRs = sheetObjectToUpdate['protectedRanges']
    except KeyError:
        print("WARNING: Sheet %s Has No Protected Ranges" % sheetObjectToUpdate['properties']['title'])
        return requests

    for PR in sheetPRs:
        if(user_to_clone not in PR['editors']['users']):
            continue
        PR['editors']['users'].append(user_to_add)
        users = PR['editors']['users']
        requests.append({
            'updateProtectedRange': {
                'protectedRange': {
                    'protectedRangeId': PR['protectedRangeId'],
                    'editors': {
                        'users': users
                    }
                },
                "fields": "editors"
            }
        })

    return requests

def compileUpdateSheetProtectedRangesAddUsers(sheetObjectToUpdate, requests, service, spreadsheet_id, startEndRowColumn, users_to_add):

    try:
        sheetPRs = sheetObjectToUpdate['protectedRanges']
    except KeyError:
        print("WARNING: Sheet %s Has No Protected Ranges" % sheetObjectToUpdate['properties']['title'])
        return requests

    for PR in sheetPRs:
        try:

            if((PR['range']['startRowIndex'] == startEndRowColumn[0]) and (PR['range']['endRowIndex'] == startEndRowColumn[1]) and (PR['range']['startColumnIndex'] == startEndRowColumn[2]) and (PR['range']['endColumnIndex'] == startEndRowColumn[3])):
                PR['editors']['users'].extend(users_to_add)
                users = PR['editors']['users']
                requests.append({
                    'updateProtectedRange': {
                        'protectedRange': {
                            'protectedRangeId': PR['protectedRangeId'],
                            'editors': {
                                'users': users
                            }
                        },
                        "fields": "editors"
                    }
                })
        except KeyError:
            continue

    return requests

def compileFormatCells_HorizontalAlign(sheetId, requests, startEndRowColumn, service, spreadsheet_id, horizAlign = "CENTER"): 

    requests.append({
        'repeatCell': {
            'range': {
                'sheetId': sheetId,
                'startRowIndex': startEndRowColumn[0],
                'endRowIndex': startEndRowColumn[1],
                'startColumnIndex': startEndRowColumn[2],
                'endColumnIndex': startEndRowColumn[3]
            },
            'cell': {
                'userEnteredFormat': {'horizontalAlignment': horizAlign}
            },
            'fields': 'userEnteredFormat(horizontalAlignment)'
        }
    })

    return requests

def compileFormatCells_VerticalAlign(sheetId, requests, startEndRowColumn, service, spreadsheet_id, vertAlign = "MIDDLE"): 

    requests.append({
        'repeatCell': {
            'range': {
                'sheetId': sheetId,
                'startRowIndex': startEndRowColumn[0],
                'endRowIndex': startEndRowColumn[1],
                'startColumnIndex': startEndRowColumn[2],
                'endColumnIndex': startEndRowColumn[3]
            },
            'cell': {
                'userEnteredFormat': {'verticalAlignment': vertAlign}
            },
            'fields': 'userEnteredFormat(verticalAlignment)'
        }
    })

    return requests

def compileChangeRowHeight(sheetId, requests, startEndRowColumn, service, spreadsheet_id, pixelHeight = 100): 

    requests.append({
        'updateDimensionProperties': {
            'range': {
                'sheetId': sheetId,
                'dimension': "ROWS",
                'startIndex': startEndRowColumn[0],
                'endIndex': startEndRowColumn[1]
            },
            'properties': {
                'pixelSize': pixelHeight
            },
            'fields': 'pixelSize'
        }
    })

    return requests

def compileUpdateProtectedRangeAddUser(sheetObjectToUpdate, requests, rangeID, service, spreadsheet_id, user_to_add): 

    sheetPRs = sheetObjectToUpdate['protectedRanges']

    for PR in sheetPRs:
        if (PR['protectedRangeId'] == rangeID):
            PR['editors']['users'].append(user_to_add)
            users = PR['editors']['users']
    
    requests.append({
        'updateProtectedRange': {
            'protectedRange': {
                'protectedRangeId': rangeID,
                'editors': {
                    'users': users
                }
            },
            "fields": "editors"
        }
    })

    return requests

def compileUpdateProtectedRangeAddUnprotectedRangeToSheet(sheetObjectToUpdate, requests, service, spreadsheet_id, rangeToAdd): 

    sheetPRs = sheetObjectToUpdate['protectedRanges']

    for PR in sheetPRs:
        if 'unprotectedRanges' in PR:
            rangeID = PR['protectedRangeId']
            PR['unprotectedRanges'].append({'sheetId': sheetObjectToUpdate['properties']['sheetId'], 'startRowIndex': rangeToAdd[0], 'endRowIndex': rangeToAdd[1],\
             'startColumnIndex': rangeToAdd[2], 'endColumnIndex': rangeToAdd[3]})
            unprotectedRanges = PR['unprotectedRanges']

    requests.append({
        'updateProtectedRange': {
            'protectedRange': {
                'protectedRangeId': rangeID,
                'unprotectedRanges': unprotectedRanges
            },
            "fields": "unprotectedRanges"
        }
    })

    return requests

def compileFormatCells_TextFormat(sheetId, requests, startEndRowColumn, service, spreadsheet_id, textFormat = {'bold': True, 'fontFamily': 'Helvetica'}): 

    requests.append({
        'repeatCell': {
            'range': {
                'sheetId': sheetId,
                'startRowIndex': startEndRowColumn[0],
                'endRowIndex': startEndRowColumn[1],
                'startColumnIndex': startEndRowColumn[2],
                'endColumnIndex': startEndRowColumn[3]
            },
            'cell': {
                'userEnteredFormat': {'textFormat': textFormat}
            },
            'fields': 'userEnteredFormat(TextFormat)'
        }
    })

    return requests

def compileMergeCells(sheetId, requests, startEndRowColumn, service, spreadsheet_id, mergeType = "MERGE_ROWS"): 

    requests.append({
        'mergeCells': {
            'range': {
                'sheetId': sheetId,
                'startRowIndex': startEndRowColumn[0],
                'endRowIndex': startEndRowColumn[1],
                'startColumnIndex': startEndRowColumn[2],
                'endColumnIndex': startEndRowColumn[3]
            },
            'mergeType': mergeType
        }
    })

    return requests

def compileBordersBox(sheetId, requests, startEndRowColumn, service, spreadsheet_id): 

    requests.append({
        'updateBorders': {
            'range': {
                'sheetId': sheetId,
                'startRowIndex': startEndRowColumn[0],
                'endRowIndex': startEndRowColumn[1],
                'startColumnIndex': startEndRowColumn[2],
                'endColumnIndex': startEndRowColumn[3]
            },
            "top": {
                "style": "SOLID",
                "width": 1,
                "color": {
                    "red": 0.0,
                    "blue": 0.0,
                    "green": 0.0
                }
            },
            "bottom": {
                "style": "SOLID",
                "width": 1,
                "color": {
                    "red": 0.0,
                    "blue": 0.0,
                    "green": 0.0
                }
            },
            "left": {
                "style": "SOLID",
                "width": 1,
                "color": {
                    "red": 0.0,
                    "blue": 0.0,
                    "green": 0.0
                }
            },
            "right": {
                "style": "SOLID",
                "width": 1,
                "color": {
                    "red": 0.0,
                    "blue": 0.0,
                    "green": 0.0
                }
            }
        }
    })

    return requests

def compileRepeatCellRequest(sheetId, requests, startEndRowColumn, service, spreadsheet_id, value):

    requests.append({
        'repeatCell': {
            'range': {
                'sheetId': sheetId,
                    'startRowIndex': startEndRowColumn[0],
                    'endRowIndex': startEndRowColumn[1],
                    'startColumnIndex': startEndRowColumn[2],
                    'endColumnIndex': startEndRowColumn[3]
            },
            'cell': {
                'userEnteredValue': {
                    'formulaValue': value
                }
            },
            'fields': 'userEnteredValue'
        }
    })
    
    return requests

def compileChangeColumnWidth(sheetId, requests, startEndRowColumn, service, spreadsheet_id, pixelWidth = 100): 

    requests.append({
        'updateDimensionProperties': {
            'range': {
                'sheetId': sheetId,
                'dimension': "COLUMNS",
                'startIndex': startEndRowColumn[2],
                'endIndex': startEndRowColumn[3]
            },
            'properties': {
                'pixelSize': pixelWidth
            },
            'fields': 'pixelSize'
        }
    })

    return requests

def compileAddSheetRequest(sheet_name, tabColor, spreadsheetInfo, requests, service, spreadsheet_id, sheetID = "000000099"):

    try:
        sheet_title = next(sheet['properties']['title'] for sheet in spreadsheetInfo['sheets'] if sheet['properties']['title'] == sheet_name)
        print("WARNING: Sheet %s Already Exists" % sheet_title)
        sheetID = next(sheet['properties']['sheetId'] for sheet in spreadsheetInfo['sheets'] if sheet['properties']['title'] == sheet_name)
    except StopIteration:
        requests.append({
            'addSheet': {
                'properties': {
                    'title': sheet_name,
                    'tabColor': tabColor,
                    'sheetId': sheetID
                }
            }
        })

    return [requests, sheetID]

def compileWrapStrategy(sheetId, requests, startEndRowColumn, service, spreadsheet_id, wrapStrategy = "WRAP"):

    requests.append({
        'repeatCell': {
            'range': {
                'sheetId': sheetId,
                    'startRowIndex': startEndRowColumn[0],
                    'endRowIndex': startEndRowColumn[1],
                    'startColumnIndex': startEndRowColumn[2],
                    'endColumnIndex': startEndRowColumn[3]
            },
            'cell': {
                'userEnteredFormat': {
                    'wrapStrategy': wrapStrategy
                }
            },
            "fields": "userEnteredFormat(wrapStrategy)"
        }
    })

    return requests

def compileFormatCells_TextColor(sheetId, requests, startEndRowColumn, service, spreadsheet_id, textColor = COLOR_BLACK): 

    requests.append({
        'repeatCell': {
            'range': {
                'sheetId': sheetId,
                'startRowIndex': startEndRowColumn[0],
                'endRowIndex': startEndRowColumn[1],
                'startColumnIndex': startEndRowColumn[2],
                'endColumnIndex': startEndRowColumn[3]
            },
            'cell': {
                'userEnteredFormat': {
                    'textFormat': {
                        'foregroundColor': textColor
                    }
                }
            },
            'fields': 'userEnteredFormat(TextFormat)'
        }
    })

    return requests

def compileFormatCells_NumberFormat(sheetId, requests, startEndRowColumn, service, spreadsheet_id, numFormat = "NUMBER"): 

    requests.append({
        'repeatCell': {
            'range': {
                'sheetId': sheetId,
                'startRowIndex': startEndRowColumn[0],
                'endRowIndex': startEndRowColumn[1],
                'startColumnIndex': startEndRowColumn[2],
                'endColumnIndex': startEndRowColumn[3]
            },
            'cell': {
                'userEnteredFormat': {
                    'numberFormat': {
                        'type': numFormat
                    }
                }
            },
            'fields': 'userEnteredFormat(numberFormat)'
        }
    })

    return requests

def compileAddDimensionToSheet(sheetId, requests, service, spreadsheet_id, dimension = "ROWS", countToAppend = 1000): 

    requests.append({
        'appendDimension': {
            'sheetId': sheetId,
            'dimension': dimension,
            'length': countToAppend
        }
    })

    return requests

def readValuesFromSheet(sheet_to_read, range_to_read, service, majorDimension = "ROWS"):

    sheetData = service.spreadsheets().values().get(spreadsheetId = sheet_to_read, range = range_to_read, majorDimension = majorDimension).execute()

    return sheetData['values']


def generateSpreadsheet(towers, N_45_TRUCKS, N_16_TRUCKS, service, spreadsheet_id):

    #\'Check In-45 ft Run 1\'!B2+\'Check In-45 ft Run 2\'!B2+\'Check In-45 ft Run 3\'!B2+\'Check In-16 ft Run 1\'!B2+\'Check In-16 ft Run 2\'!B2
    towerCheckOffString = ''
    truckTitleValues = [
        ['', '', '', '','','','','']
    ]

    #Set Up Truck Runs for the Week, Correct Cell References, and Hide Check-In Sheets, Build Tower Check Off String, List Trucks in truckTitleValues, and remap cells in Shipping Summary to draw from correct check-in sheets
    for i in range(int(N_45_TRUCKS)):
        print(duplicateSheet('45 ft Truck Map', '45 ft Run ' + str(N_45_TRUCKS-i).split(".")[0], service, spreadsheet_id))
        print(duplicateSheet('Check In-45 ft', 'Check In-45 ft Run ' + str(N_45_TRUCKS-i).split(".")[0], service, spreadsheet_id))
        print(findReplace('Check In-45 ft Run ' + str(N_45_TRUCKS-i).split(".")[0], '45 ft Truck Map', '45 ft Run ' + str(N_45_TRUCKS-i).split(".")[0], True, service, spreadsheet_id))
        print(hideSheet('Check In-45 ft Run ' + str(N_45_TRUCKS-i).split(".")[0], service, spreadsheet_id))
        towerCheckOffString += '+\'Check In-45 ft Run ' + str(N_45_TRUCKS-i).split(".")[0] + '\'!B2'
        truckTitleValues[0][int(N_45_TRUCKS-i-1)] = '45 ft Truck '+ str(N_45_TRUCKS-i).split(".")[0]

        shippingSummaryString = '=IF(SUM(\'Check In-45 ft Run ' + str(N_45_TRUCKS-i).split(".")[0] + '\'!$B2:$AE2)>0,SUM(\'Check In-45 ft Run ' + str(N_45_TRUCKS-i).split(".")[0] + '\'!$B2:$AE2),"")'
        print(repeatCell('Shipping Summary', 2, 100, int(N_45_TRUCKS-i), int(N_45_TRUCKS-i+1), shippingSummaryString, service, spreadsheet_id))



    for i in range(int(N_16_TRUCKS)):
        print(duplicateSheet('16 ft Truck Map', '16 ft Run ' + str(N_16_TRUCKS-i).split(".")[0], service, spreadsheet_id))
        print(duplicateSheet('Check In-16 ft', 'Check In-16 ft Run ' + str(N_16_TRUCKS-i).split(".")[0], service, spreadsheet_id))
        print(findReplace('Check In-16 ft Run ' + str(N_16_TRUCKS-i).split(".")[0], '16 ft Truck Map', '16 ft Run ' + str(N_16_TRUCKS-i).split(".")[0], True, service, spreadsheet_id))
        print(hideSheet('Check In-16 ft Run ' + str(N_16_TRUCKS-i).split(".")[0], service, spreadsheet_id))
        towerCheckOffString += '+\'Check In-16 ft Run ' + str(N_16_TRUCKS-i).split(".")[0] + '\'!B2'
        truckTitleValues[0][int(N_16_TRUCKS+N_45_TRUCKS-i-1)] = '16 ft Truck '+ str(N_16_TRUCKS-i).split(".")[0]

        shippingSummaryString = '=IF(SUM(\'Check In-16 ft Run ' + str(N_16_TRUCKS-i).split(".")[0] + '\'!$B2:$AE2)>0,SUM(\'Check In-16 ft Run ' + str(N_16_TRUCKS-i).split(".")[0] + '\'!$B2:$AE2),"")'
        print(repeatCell('Shipping Summary', 2, 100, int(N_16_TRUCKS+N_45_TRUCKS-i), int(N_16_TRUCKS+N_45_TRUCKS-i+1), shippingSummaryString, service, spreadsheet_id))
    
    print(writeValues("Shipping Summary!B2:I2",truckTitleValues,service,spreadsheet_id))


    #Now, re-map the Tower Check-Off master summary sheet. It needs to reference only the active truck-map check-in sheets
    towerCheckOffString = '=IF(' + towerCheckOffString[1:] + '=0,"",' + towerCheckOffString[1:] +')'
    print(repeatCell('Tower Check-Off', 2, 100, 1, 50, towerCheckOffString, service, spreadsheet_id))

    #Next,color the background as red for each of the meals and frozen components
    towerValues = []
    for i in range(len(towers)):
        towerValues.append([towers[i]['id']])
        print(colorCellRangeRed('Tower Check-Off', 2+i, 3+i, 1, towers[i]['count']+1, service, spreadsheet_id))
    
    print(writeValues("Reference!A2:A" + str(len(towers)+1),towerValues,service,spreadsheet_id))

    #Finally, clean hide all of the remaining sheets that the production team doesn't need to see
    print(hideSheet('Check In-45 ft', service, spreadsheet_id))
    print(hideSheet('Check In-16 ft', service, spreadsheet_id))
    print(hideSheet('45 ft Truck Map', service, spreadsheet_id))
    print(hideSheet('16 ft Truck Map', service, spreadsheet_id))
    print(hideSheet('Reference', service, spreadsheet_id))

