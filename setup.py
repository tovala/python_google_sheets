import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="google_sheets",
    version="0.4.1",
    author="Peter Fiflis",
    author_email="peter@tovala.com",
    description="Module for interacting with Google Sheets",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/tovala/python_google_sheets",
    packages=setuptools.find_packages(exclude=['test*']),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    include_package_data=True,
    install_requires=[
          'numpy',
          'scipy',
          'urllib3',
          'datetime',
          'tovala_utilities'
    ]
)